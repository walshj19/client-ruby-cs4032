require 'socket'
require 'zlib'
require 'openssl'

class Client
	# declare the instance variables for the class
	def initialize
		@host = 'www.scss.tcd.ie'												# The sever address
		@port = 443																			# Default HTTP port
		@path = "/~ebarrett/lectures/cs4032/echo.php"		# The file location
		@message = "success"

		# http headers
		@send_headers = {
			"Host" => @host,
			"Connection" => "close",
			"User-Agent" => "MyAwesomeUserAgent/1.0.0",
			"Accept-Encoding" => "gzip",
			"Accept-Charset" => "ISO-8859-1,UTF-8;q=0.7,*;q=0.7",
			"Cache-Control" => "no-cache"
		}

		@del = "\r\n"
	end

	# setup the tcp connection and make the get request
	def makeCall
		# Generate the request
		request = "GET #{@path}?message=#{@message} HTTP/1.1#{@del}"
		@send_headers.each do |key, value|
			request << "#{key}: #{value}#{@del}"
		end
		request << "#{@del}"

		print "---REQUEST---\n" << request

		# start the connection and send the request
		tcp_socket = TCPSocket.open(@host,@port)						# Connect to server
		ssl_client = OpenSSL::SSL::SSLSocket.new tcp_socket	# Start SSL
		ssl_client.connect																	# Authenticate
		ssl_client.print request														# Send request
		response = ssl_client.read													# Read complete response

		# Split response at first blank line into headers and body
		response_headers,body = response.split("\r\n\r\n", 2)

		print "---RESPONSE---\n"+response_headers+"\n\n"

		decompress body
	end

	# decompress the body of the response and print it
	def decompress(data)
		file = File.new("response.gz", "w+")
		file.write data
		file.close

		File.open('response.gz') do |f|
			gz = Zlib::GzipReader.new(f)
			print "---BODY---\n"+gz.read+"\n"
			gz.close
		end
	end
end

# start the program
Client.new.makeCall
